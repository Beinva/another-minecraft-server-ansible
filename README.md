Another Minecraft server with ansible
---
`State` : working

Just a simple playbook for install a minecraft server and overviewer map. 

### Requirement for ansible: 
- ssh
- python
- sudo

### playbook functionality

- UpdateOS
  + Debian: Check! 
- Install minecraft required
  + Debian: Check!
- Install minecraft
  + Denian: Check!
- Update minecraft
  + Debian: NotCheck!
- Install overviewer
  + Debian: Check!

