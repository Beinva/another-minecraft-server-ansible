worlds["My world"]="{{ home_minecraft }}{{ workingdir }}/world"
outputdir="{{ overviewer_output }}"
rendermode="smooth_lighting"

#daylight
renders["daywolrd"]={
	"world": "My world",
	"title": "Daylight",
}

#daynight
renders["daynight"]={
	'world': "My world",
	'title': "Nighttime",
	'rendermode': 'smooth_night',
}

