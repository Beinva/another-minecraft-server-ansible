[Unit]
Description=Minecraft Server
After=network.target

[Service]
User={{ user_minecraft }}
Nice=1
KillMode=none
SuccessExitStatus=0 1
ProtectHome=true
ProtectSystem=full
PrivateDevices=true
NoNewPrivileges=true
WorkingDirectory={{ home_minecraft }}/{{ workingdir }}
ExecStart=/usr/bin/java -Xmx{{ max_memory }}M -Xms{{ min_memory }}M -jar server.jar nogui
ExecStop=/opt/minecraft/tools/mcrcon/mcrcon -H {{ host_mcon }} -P {{ port_mcon }} -p {{ pass_mcon }} stop

[Install]
WantedBy=multi-user.target
